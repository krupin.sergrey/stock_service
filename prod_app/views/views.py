from flask_restful import Resource, url_for, reqparse
from flask import current_app, send_file
import sqlite3 
import werkzeug
import datetime
import os
from .. import main


parser = reqparse.RequestParser()
parser.add_argument('id')
parser.add_argument('name')
parser.add_argument('sku')
parser.add_argument('group')
parser.add_argument('quantity')


class ItemProduct(Resource):
    ''' Работаем с текущем товаром'''
        
    def post(self):
        '''
        Add new product
        POST /api/v1.0/product
        '''
        args = parser.parse_args()
        db = main.db.get_db()

        if not args['name']: return {'message': 'Empty name product'}, 400
        if not args['sku']: return {'message': 'Empty sku product'}, 400
        if not args['group']: return {'message': 'Empty product group'}, 400

        checked_id = db.execute('SELECT id FROM groups WHERE name=?', (args['group'],)).fetchone()
        if not checked_id:
            try:
                db.execute('INSERT INTO groups  VALUES (NULL, ?)',
                            (args['group'],))
                db.commit()
                checked_id = db.execute('SELECT id FROM groups WHERE name=?', (args['group'],)).fetchone()
            except sqlite3.Error as error:
                return {'message': error.args}, 500
        try:
            db.execute('INSERT INTO products (name, sku, groups_id) VALUES (?, ?, ?)',
                            (args['name'], args['sku'], checked_id[0]))
            db.commit()
            return {'message':'product append'}, 201
        except sqlite3.IntegrityError as error:
            return {'message': error.args}, 400

        except sqlite3.Error as error:
            return {'message': error.args}, 500
    
    def put(self, product_id):
        '''
        Change quantity product with id
        PUT /api/v1.0/product/<int:product_id>
        '''
        args = parser.parse_args()

        if not args['quantity']: return {'message': 'Empty quantity'}, 400
        if int(args['quantity']) < 0: return {'message': 'Bad quantity'}, 400  

        db = main.db.get_db()
        checked_id = db.execute('SELECT id FROM products WHERE id=?', (product_id,)).fetchone()
        if checked_id:
            try:
                db.execute('UPDATE products SET quantity=? WHERE id=?',
                            (args['quantity'], product_id,))
                db.commit()
                return {'message':'Quantity product change'}, 200
            except sqlite3.Error as error:
                return {'message': error}, 500
        return {'message': 'Product not found'}, 404


    def delete(self, product_id):
        '''
        Delete product with id
        DELETE /api/v1.0/product/<int:product_id>
        '''
        db = main.db.get_db()
        checked_id = db.execute('SELECT id FROM products WHERE id=?', (product_id,)).fetchall()
        if checked_id:
            try:
                db.execute('DELETE FROM products WHERE id=?',
                            (product_id,))
                db.commit()
                return {'message':'Product delete'}, 200
            except sqlite3.Error as error:
                return {'message': 'error'}, 500
        return {'message': 'Product not found'}, 404
            

class Groups(Resource):
    '''Работаем с группами товаров'''

    def prod_to_json(self, prod:tuple)->dict:
        id, name, sku, quantity = prod
        return {'ID': id,
                'name': name,
                'sku': sku,
                'quantity': quantity
               }

    def get(self, group_id):
        '''
        Get a list of products in a group (id) 
        GET /api/v1.0/group/<int:group_id>
        '''

        db = main.db.get_db()
        name = db.execute('SELECT name FROM groups WHERE id=?', (group_id,)).fetchone()
        if name:
            products = db.execute('SELECT id, name, sku, quantity FROM products WHERE groups_id=?',
                                 (group_id,)).fetchall()
            return {'group ID': group_id,
                    'group name': name[0],
                    'products': [self.prod_to_json(prod) for prod in products],
                    'product list url': url_for('bp.groups', group_id=group_id)
                    }
        return {'message': 'Group not found'}, 404


class GroupsHelp(Resource):
    '''Работаем с группами товаров на случай когда неизвестен id группы'''

    def prod_to_json(self, prod:tuple)->dict:
        id, name,  = prod
        return {'ID': id,
                'name': name,
                'product list url': url_for('bp.groups', group_id=id)
               }

    def get(self):
        '''
        Get a list of group 
        GET /api/v1.0/group
        '''

        db = main.db.get_db()
        groups = db.execute('SELECT id, name FROM groups').fetchall()
        if groups:
            return {'groups': [self.prod_to_json(group) for group in groups],
                    'groups list url': url_for('bp.groupshelp')
                    }
        return {'message': 'Groups not found'}, 404