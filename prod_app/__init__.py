import os
from flask import Flask, Blueprint,jsonify
from flask_restful import Api


def create_app(test_config=None):

    app = Flask(__name__, instance_relative_config=True )

    try:
        os.makedirs(os.path.join(app.instance_path, 'resources', 'db'))
    except OSError:
        pass

    app.config.from_mapping(
        DATABASE = os.path.join(app.instance_path, 'resources', 'db', 'src.sqlite'),
    )
    
    if test_config is None:
        app.config.from_pyfile('config.py', silent=True)
    else:
        app.config.from_mapping(test_config)

    bp_views = Blueprint('bp', __name__, url_prefix='/api/v1.0')
    
    from .views.views import ItemProduct, Groups, GroupsHelp 
    from .views import errors
    
    api = Api(bp_views)

    
    api.add_resource(ItemProduct, '/product/<int:product_id>')
    api.add_resource(ItemProduct, '/product', endpoint='product')
    api.add_resource(Groups, '/group/<int:group_id>')
    api.add_resource(GroupsHelp, '/group')



    app.register_blueprint(bp_views)
    app.register_error_handler(400, errors.bad_request)
    app.register_error_handler(404, errors.page_not_found)
    app.register_error_handler(500, errors.internal_server_error)

    from .main import db
    db.init_app(app)


    return app