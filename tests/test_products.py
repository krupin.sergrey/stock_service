
def test_add_new_product(client):
    '''
    Тест на добавление нового продукта.
    Корректный запрос.
    '''
   
    response = client.post('/api/v1.0/product',
                           data={'name':'Скоба кованная',
                                 'sku':'01020303002',
                                 'group':'скобы'})    

    assert '201' in  response.status           

    ''',
    Некорректный запрос.
    Товар с таким наименованием уже существует.
    '''
    
    response = client.post('/api/v1.0/product',
                           data={'name':'шуруп оцинкованный',
                                 'sku':'01020301004',
                                 'group':'шурупы'})    

    assert '400' in  response.status 
    
def test_delete_product(client):
    '''
    Тест на удаление товара  с id 1.
    Корректный запрос.
    '''
   
    response = client.delete('/api/v1.0/product/1')    
    assert '200' in  response.status           

    '''
    Некорректный запрос.
    Товара с таким id не существует.
    '''
    
    response = client.delete('/api/v1.0/product/99')    
    assert '404' in  response.status 
    


def test_change_product(client):
    '''
    Тест на изменение продукта с id 2.
    Корректный запрос.
    '''
   
    response = client.put('/api/v1.0/product/2',
                           data={'quantity':'22'})    

    assert {'message': 'Quantity product change'} ==  response.get_json()         

    '''
    Некорректный запрос.
    Товара с таким id не существует.
    '''
    
    response = client.put('/api/v1.0/product/99',
                           data={'quantity':'23'})    

    assert '404' in  response.status 