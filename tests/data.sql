INSERT INTO groups (name)
VALUES
  ('шурупы'),
  ('гвозди'),
  ('скобы');


INSERT INTO products (name, sku, groups_id)
VALUES
  ('шуруп оцинкованный', '01020301001', (SELECT id FROM groups WHERE name='шурупы')),
  ('шуруп обыкновенный', '01020301002', (SELECT id FROM groups WHERE name='шурупы')),
  ('шуруп кровельный', '01020301003', (SELECT id FROM groups WHERE name='шурупы')),
  ('гвоздь обыкновенный', '01020302001', (SELECT id FROM groups WHERE name='гвозди')),
  ('гвоздь медный', '01020302002', (SELECT id FROM groups WHERE name='гвозди'));

