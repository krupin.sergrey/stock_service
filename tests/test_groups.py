import os


def test_get_prod_collection_by_id(client):
    '''
    Тест вывода коллекции товара для группы с id.
    Корректный запрос.
    '''
    rv = client.get('/api/v1.0/group/1')
    assert 'шурупы' == rv.get_json()['group name']

    '''
    Некорректный запрос.
    '''
    rv = client.get('/api/v1.0/group/99')
    assert {'message': 'Group not found'} == rv.get_json()


def test_get_groups_collection(client):
    '''
    Тест вывода коллекции групп товаров.
    '''
    rv = client.get('/api/v1.0/group')
    assert ['шурупы', 'гвозди', 'скобы'] == [group['name'] for group in rv.get_json()['groups']]

    
