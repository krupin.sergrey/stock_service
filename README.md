Приложение построено на функциональности CRUD.

| Действие | Метод  | URL                                  | Описание                                                     |
|----------|--------|--------------------------------------|--------------------------------------------------------------|
| Create   | POST   | /api/v1.0/product                    | Добавить товар                                               |
| Read     | GET    | /api/v1.0/group/*<int:group_id>*     | Вывод списка товаров по группе с id                          |
| Read     | GET    | /api/v1.0/group                      | Вывод списка групп                                           |
| Update   | PUT    | /api/v1.0/product/*<int:product_id>* | Изменить изменить информацию о количестве продукта по его id |
| Delete   | DELETE | /api/v1.0/product/*<int:comment_id>* | Удалить продукт по его id                                    | 


Этапы работы с сервером.

Инициализация БД. 
```sh
$ export FLASK_APP=prod_app
$ flask init-db
```

Запуск сервера.
```sh
$ export FLASK_APP=prod_app
$ export FLASK_ENV=development
$ flask run
```


Тестирование.
```sh
$ python -m pytest

=========================================== test session starts ============================================
platform linux -- Python 3.8.5, pytest-5.4.3, py-1.9.0, pluggy-0.13.1 -- /home/serg/devman/stock_service/venv/bin/python
cachedir: .pytest_cache
rootdir: /home/serg/devman/stock_service
collected 8 items                                                                                          

tests/test_db.py::test_get_close_db PASSED                                                           [ 12%]
tests/test_db.py::test_init_db_command PASSED                                                        [ 25%]
tests/test_factory.py::test_config PASSED                                                            [ 37%]
tests/test_groups.py::test_get_prod_collection_by_id PASSED                                          [ 50%]
tests/test_groups.py::test_get_groups_collection PASSED                                              [ 62%]
tests/test_products.py::test_add_new_product PASSED                                                  [ 75%]
tests/test_products.py::test_delete_product PASSED                                                   [ 87%]
tests/test_products.py::test_change_product PASSED                                                   [100%]

============================================ 8 passed in 0.46s =============================================
```
Тестированием выполняются следующие проверки:
1. Передача конфигурации в фабричную функцию.
2. Инициализация БД с командной строки (flask init-db)
3. Вывод коллекции товаров принадлежащие определенной группе. Два запроса, корректный/некорректный.
4. Вывод коллекции групп товаров. Как правило используется, чтоб найти id интересуюемой группы. Url о группе отдается в ответе.
5. Добавить новый товар. Корректный - уникальное имя и sku. Некорректный - товар с таким именованием существует.
6. Удалить товар по его id. Проверка некорректного запроса - неверно указан id.
7. Обновить информацию о товаре по его id. Проверка некорректного запроса - неверно указан id.
